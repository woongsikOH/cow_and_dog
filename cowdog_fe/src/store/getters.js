export function getNotices (state) {
	return state.notices
}

export function getUserId(state){
	return state.login_user
}

export function getUserToken(state){
	return state.access_token
}